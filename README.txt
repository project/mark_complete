
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Credits


INTRODUCTION
------------

The Mark Complete module enables you to click a link on a node display page
that will update a date field to the current date. This is done via AJAX so
there is no page refresh needed.

In the settings page, you can specify which date fields will show the Mark
Complete links. This makes it easy to provide the functionality on any content
type or datestamp field, rather than just hard-coding it for articles.

There is also support for multiple delta items, so that each item has a link
and clicking the link of one won't affect the others.

You can view a demonstration here: http://www.youtube.com/watch?v=Y3nEcsCqmHQ


INSTALLATION
------------

Installation is typical of a Drupal module.

1. This module requires the Date module to work
   (http://www.drupal.org/project/date).

2. Copy this module to your sites/SITENAME/modules directory.

3. Enable the module.

4. Set up the permissions so that users with admin roles can use the links.

5. Configure the module at admin/config/content/mark_complete to set which
   fields will use the mark complete links.


CREDITS
-------

This module was developed by Leighton Whiting as an entry for the ModuleOff.com
contest here:

http://moduleoff.com/contest/add-link-every-article-node-updates-date-field-today-using-ajax

The contest was sponsored by Merlin Education LLC (http://merlineducation.com/)
